﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(domi.Backend.Startup))]
namespace domi.Backend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
